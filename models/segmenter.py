import tensorflow as tf
import tensorflow.keras as tfk
import tensorflow.keras.layers as tfkl


def reseg_net(input_shape=(152, 152, 184, 1),
              base_filters=128,
              conv_activation=tf.nn.leaky_relu) -> tfk.Model:
    assert base_filters % 2 == 0
    input_ = tfkl.Input(shape=input_shape, dtype="float32", name="layer_0/input")

    layer_num = 0
    trim_1 = tfkl.Conv3D(filters=base_filters,
                         kernel_size=(9, 9, 9),
                         strides=(2, 2, 2),
                         activation=conv_activation,
                         kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                         bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                         padding="same",
                         name="layer_{}/conv777".format(layer_num))(input_)
    layer_num = 1

    trim_2 = tfkl.Conv3D(filters=2 * base_filters,
                         kernel_size=(7, 7, 7),
                         activation=conv_activation,
                         kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                         bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                         padding="same",
                         name="layer_{}/conv777".format(layer_num))(trim_1)
    trim_2 = tfkl.Conv3D(filters=2 * base_filters,
                         kernel_size=(5, 5, 5),
                         strides=(2, 2, 2),
                         activation=conv_activation,
                         kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                         bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                         padding="same",
                         name="layer_{}/conv555".format(layer_num))(trim_2)
    trim_2 = tfkl.BatchNormalization(name="layer_{}/batchnorm".format(layer_num))(trim_2)
    trim_2 = tfkl.Activation(name="layer_{}/activation".format(layer_num), activation=conv_activation)(trim_2)

    layer_num = 2
    x = tfkl.Conv3D(filters=base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_1".format(layer_num))(trim_2)
    x = tfkl.Conv3D(filters=2 * base_filters,
                    kernel_size=(3, 3, 3),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_2".format(layer_num))(x)
    x = tfkl.Conv3D(filters=2 * base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_3".format(layer_num))(x)
    trim_3 = tfkl.Conv3D(filters=4 * base_filters,
                         kernel_size=(3, 3, 3),
                         strides=(2, 2, 2),
                         kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                         bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                         padding="same",
                         name="layer_{}/conv_4".format(layer_num))(x)
    trim_3 = tfkl.BatchNormalization(name="layer_{}/batchnorm".format(layer_num))(trim_3)
    trim_3 = tfkl.Activation(name="layer_{}/activation".format(layer_num), activation=conv_activation)(trim_3)

    ## bottleneck
    layer_num = 3
    x = tfkl.Conv3D(filters=2 * base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_1".format(layer_num))(trim_3)
    x = tfkl.Conv3D(filters=4 * base_filters,
                    kernel_size=(3, 3, 3),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_2".format(layer_num))(x)
    x = tfkl.Conv3D(filters=2 * base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_3".format(layer_num))(x)
    x = tfkl.Conv3D(filters=4 * base_filters,
                    kernel_size=(3, 3, 3),
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_4".format(layer_num))(x)
    x = tfkl.BatchNormalization(name="layer_{}/batchnorm1".format(layer_num))(x)
    x = tfkl.Activation(name="layer_{}/activation1".format(layer_num), activation=conv_activation)(x)
    x = tfkl.Conv3D(filters=2 * base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_5".format(layer_num))(x)
    x = tfkl.Conv3D(filters=4 * base_filters,
                    kernel_size=(3, 3, 3),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_6".format(layer_num))(x)
    x = tfkl.Conv3D(filters=4 * base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_7".format(layer_num))(x)
    x = tfkl.Conv3D(filters=8 * base_filters,
                    kernel_size=(3, 3, 3),
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_8".format(layer_num))(x)
    x = tfkl.BatchNormalization(name="layer_{}/batchnorm2".format(layer_num))(x)
    x = tfkl.Activation(name="layer_{}/activation2".format(layer_num), activation=conv_activation)(x)
    ## upsampling
    layer_num = 4

    x = tfkl.Conv3D(filters=4 * base_filters,
                    kernel_size=(3, 3, 3),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_1".format(layer_num))(x)
    x = tfkl.Concatenate(name="layer_{}/concat1".format(layer_num))([trim_3, x])

    x = tfkl.Conv3DTranspose(filters=2 * base_filters,
                             kernel_size=(2, 2, 2),
                             strides=(2, 2, 2),
                             output_padding=(1, 1, 1),
                             activation=conv_activation,
                             kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                             bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                             padding="same",
                             name="layer_{}/conv_2".format(layer_num))(x)

    x = tfkl.Concatenate(name="layer_{}/concat2".format(layer_num))([trim_2, x])
    layer_num = 5
    x = tfkl.Conv3D(filters=2 * base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_1".format(layer_num))(x)
    x = tfkl.Conv3D(filters=base_filters,
                    kernel_size=(3, 3, 3),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_2".format(layer_num))(x)
    x = tfkl.Conv3D(filters=2 * base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_3".format(layer_num))(x)
    x = tfkl.Conv3DTranspose(filters=2 * base_filters,
                             kernel_size=(2, 2, 2),
                             strides=(2, 2, 2),
                             kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                             bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                             padding="same",
                             name="layer_{}/conv_4".format(layer_num))(x)
    x = tfkl.BatchNormalization(name="layer_{}/batchnorm".format(layer_num))(x)
    x = tfkl.Activation(name="layer_{}/activation".format(layer_num), activation=conv_activation)(x)

    x = tfkl.Concatenate(name="layer_{}/concat".format(layer_num))([trim_1, x])
    layer_num = 6
    x = tfkl.Conv3D(filters=base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_1".format(layer_num))(x)
    x = tfkl.Conv3D(filters=int(0.5 * base_filters),
                    kernel_size=(3, 3, 3),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_2".format(layer_num))(x)
    x = tfkl.Conv3D(filters=base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_3".format(layer_num))(x)
    x = tfkl.Conv3DTranspose(filters=base_filters,
                             kernel_size=(2, 2, 2),
                             strides=(2, 2, 2),
                             kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                             bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                             padding="same",
                             name="layer_{}/conv_4".format(layer_num))(x)
    x = tfkl.BatchNormalization(name="layer_{}/batchnorm".format(layer_num))(x)
    x = tfkl.Activation(name="layer_{}/activation".format(layer_num), activation=conv_activation)(x)
    layer_num = 7
    x = tfkl.Conv3D(filters=base_filters,
                    kernel_size=(1, 1, 1),
                    activation=conv_activation,
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_1".format(layer_num))(x)
    x = tf.keras.layers.Concatenate(name="layer_{}/concat".format(layer_num))([x, input_])
    x = tfkl.Conv3D(filters=50,
                    kernel_size=(1, 1, 1),
                    activation="softmax",
                    kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                    padding="same",
                    name="layer_{}/conv_2".format(layer_num))(x)

    model = tfk.Model(inputs=input_, outputs=x)
    return model
