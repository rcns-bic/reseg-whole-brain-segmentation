import tensorflow as tf
import tensorflow.keras as tfk
import tensorflow.keras.layers as tfkl

mirrored_strategy = tf.distribute.MirroredStrategy()
with mirrored_strategy.scope():
    def cropper_model(base_filters,
                      conv_activation=tf.nn.swish) -> tfk.Model:
        input_ = tfkl.Input(shape=(128, 128, 128, 1), dtype="float32", name="layer_0/input")

        layer_num = 0
        x = tfkl.Conv3D(filters=base_filters,
                        kernel_size=(3, 3, 3),
                        strides=(2, 2, 2),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv1".format(layer_num))(input_)
        layer_num = 1
        x = tfkl.Conv3D(filters=2 * base_filters,
                        kernel_size=(3, 3, 3),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv1".format(layer_num))(x)
        x = tfkl.Conv3D(filters=2 * base_filters,
                        kernel_size=(3, 3, 3),
                        strides=(2, 2, 2),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv2".format(layer_num))(x)

        layer_num = 2
        x = tfkl.Conv3D(filters=4 * base_filters,
                        kernel_size=(3, 3, 3),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_1".format(layer_num))(x)
        x = tfkl.Conv3D(filters=4 * base_filters,
                        kernel_size=(3, 3, 3),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_2".format(layer_num))(x)
        x = tfkl.Conv3D(filters=4 * base_filters,
                        kernel_size=(1, 1, 1),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_3".format(layer_num))(x)
        x = tfkl.Conv3D(filters=4 * base_filters,
                        kernel_size=(3, 3, 3),
                        strides=(2, 2, 2),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_4".format(layer_num))(x)

        layer_num = 3
        x = tfkl.Conv3D(filters=8 * base_filters,
                        kernel_size=(3, 3, 3),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_1".format(layer_num))(x)
        x = tfkl.Conv3D(filters=8 * base_filters,
                        kernel_size=(3, 3, 3),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_2".format(layer_num))(x)
        x = tfkl.Conv3D(filters=8 * base_filters,
                        kernel_size=(1, 1, 1),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_3".format(layer_num))(x)
        x = tfkl.Conv3D(filters=8 * base_filters,
                        kernel_size=(3, 3, 3),
                        strides=(2, 2, 2),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_4".format(layer_num))(x)

        layer_num = 4
        x = tfkl.Conv3D(filters=8 * base_filters,
                        kernel_size=(3, 3, 3),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_1".format(layer_num))(x)
        x = tfkl.Conv3D(filters=8 * base_filters,
                        kernel_size=(3, 3, 3),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_2".format(layer_num))(x)
        x = tfkl.Conv3D(filters=8 * base_filters,
                        kernel_size=(1, 1, 1),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_3".format(layer_num))(x)
        x = tfkl.Conv3D(filters=8 * base_filters,
                        kernel_size=(3, 3, 3),
                        activation=conv_activation,
                        strides=(2, 2, 2),
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_4".format(layer_num))(x)

        layer_num = 5
        x = tfkl.Conv3D(filters=8 * base_filters,
                        kernel_size=(3, 3, 3),
                        strides=(2, 2, 2),
                        activation=conv_activation,
                        kernel_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        bias_regularizer=tf.keras.regularizers.L1L2(l1=0.01, l2=0.01),
                        padding="same",
                        name="layer_{}/conv_1".format(layer_num))(x)

        flatten = tfkl.Flatten(name="layer_{}/flatten".format(layer_num))(x)

        layer_num = 6
        x = tfkl.Dense(4096, activation=conv_activation, name="layer_{}/dense1".format(layer_num))(flatten)
        layer_num = 7
        x = tfkl.Dense(128, activation=conv_activation, name="layer_{}/dense1".format(layer_num))(x)

        layer_num = "o"
        out = tfkl.Dense(6, name="layer_{}/dense".format(layer_num))(x)

        model = tfk.Model(inputs=input_, outputs=out)

        return model
