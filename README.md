# ReSeg whole-brain segmentation

ReSeg whole-brain segmentation

<h4>Arguments:</h4>
* ```--i ```: The input folder.
* ```--o``` : Save path for masks and other output.
* ```--id_template``` : template definition to find record IDs (see example below). 
* ```--filename_template``` : Necessary if  ```--i``` folder contains subfolders. It is possible to define unknown filenames with "glob" syntax. e.g. if the subfolder contains nifti files with unknown basename: ```--filename="*.nii.gz"```
* ```--save_conformed``` : Save the conformed T1
* ```--save_entropy``` : Save the entropy array
* ```--segmenter_path``` : Path to the segmenter model (default: *./data/models/segmenter/*)
* ```--cropper_path``` : Path to the cropper model (default: *./data/models/cropper/*)

<h3>Example usage:</h3>

<h4>For folders and files:</h4>

```
data
└───test_i
│   └───id1
│   │   └───T1.nii.gz
│   └───id2
│   │   └───T1.nii.gz
│   └───id3
│       └───T1.nii.gz
└───test_o
```
<h4>Command:</h4>

    python run_reseg.py 
      --i=./data/test_i/ 
      --o=./data/test_o/ 
      --id_template="./data/test_i/__id__/T1.nii.gz"
      --filename_template="T1.nii.gz"   OR   --filename_template="*.nii.gz" 
      --save_conformed 
      --save_entropy


<h4>Models:</h4>
Model can be found at [models](https://drive.google.com/drive/folders/1qN0uGn0TP0X9cqdqB3gNCVtuR9bESZ5D?usp=sharing)
