import nibabel as nib
import numpy as np
from nibabel.freesurfer.mghformat import MGHHeader
from numpy.linalg import inv
from scipy.ndimage import affine_transform


class MriConform:
    def __init__(self, filename):
        """ Converts array to 1x1x1 mm^3 resultion and 256x256x256 voxel.
        Args:
            filename: path to nifti file
        """
        self.__correct_format = True  # if input file is in target space -> true, else false
        self.__affine_error_limit = 1e-5
        self.target_shape = (256, 256, 256)
        self.target_resolution = (1, 1, 1)

        # read nifti file
        self.inputFile = nib.load(filename=filename)

        assert (len(self.inputFile.shape) == 3 or
                (len(self.inputFile.shape) == 4 and self.inputFile.shape[3] == 1)), "Incorrect input shape!"

        # check if record is in correct space
        self.__check_correct_format()

    def conform(self, uintconvert=True, interpolation_order=3) -> np.ndarray:
        if self.__correct_format:
            return self.inputFile.get_fdata()

        input_array = self.inputFile.get_fdata()
        input_array = input_array.squeeze()
        target_header = self.get_transformed_mgz_header()

        # array to RAS
        inv_coord_m = inv(inv(target_header.get_affine()) @ np.eye(4) @ self.inputFile.affine)
        out_data = affine_transform(input=input_array,
                                    matrix=inv_coord_m,
                                    output_shape=self.target_shape,
                                    order=interpolation_order)
        # histogram intensities
        if uintconvert:
            out_data = out_data - out_data.min()
            out_data = (out_data / out_data.max()) * 255

            out_data = np.uint8(np.round(out_data, 0))
        return out_data

    def __check_correct_format(self):
        """ Checks if the record of the object is in correct space. If it is then the class variable '__correct_format'
        remains True else it will be False
        """

        # check correct voxel num
        if self.inputFile.shape[:3] != self.target_shape:
            self.__correct_format = False
            return

        # check correct resolutions
        resolutions = self.inputFile.header.get_zooms()
        if resolutions[:3] != self.target_resolution:
            self.__correct_format = False
            return

        # check correct orientation
        target_orient_neg = np.array([[1, 0, 0], [0, 0, -1], [0, 1, 0]])
        rot_m = self.inputFile.affine[0:3, 0:3]
        if np.max(np.abs(rot_m + target_orient_neg)) > self.__affine_error_limit:
            self.__correct_format = False
            return

    def get_transformed_mgz_header(self):
        if self.__correct_format:
            print("Input file is in correct format, transformation did not happen!")
            return None

        header = MGHHeader.from_header(self.inputFile.header)
        header.set_data_shape(shape=self.target_shape + (1,))
        header.set_zooms(zooms=self.target_resolution)
        header['Mdc'] = np.array([[-1, 0, 0], [0, 0, -1], [0, 1, 0]])
        header["Pxyz_c"] = self.inputFile.affine.dot(
            np.hstack((np.array(self.inputFile.get_fdata().shape[:3]) / 2.0, [1])))[:3]
        return header
