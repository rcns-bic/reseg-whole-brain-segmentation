import os


def format_and_check_path(i_path: str):
    p = i_path.replace("\\", "/")  # windows
    if p[-1] != "/":
        p = p + "/"
    return p, os.path.exists(p)


def format_file_names(i_file: str):
    f = i_file.replace("\\", "/")  # windows
    if f[0] != "/":
        f = "/" + f
    return f
