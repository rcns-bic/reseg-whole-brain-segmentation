import numpy as np
import scipy.ndimage as ndimage


def get_padded_boundaries(center, target_shape, orig_array_shape):
    """ Computes brain centered image boundaries. (If the center is too close to the original image size,
    it adjusts the center)

    brain centered is the smaller size image with centered brain

    Args:
        center (int): predicted center
        target_shape (int): target mini image shape
        orig_array_shape (int): the corresponding original iamge shape

    Returns:
        lower bound, upper bound -> crop original_image[lower:upper]
    """
    if center < target_shape / 2:
        return 0, target_shape
    elif orig_array_shape - (target_shape / 2) < center:
        return orig_array_shape - target_shape, orig_array_shape
    else:
        return center - (target_shape / 2), center + (target_shape / 2)


def batch_zoom(image: np.ndarray, zoom=0.5) -> np.ndarray:
    """ Generates mini record_from record
    \t (256 x 256 x 256 x 1) -> (1 x 128 x 128 x 128 x 1) \n
    \t OR \n
    \t (k x 256 x 256 x 256 x 1) -> (k x 128 x 128 x 128 x 1) \n
    Args:
        image: image
        zoom (float): Image resize zoom parameter

    Returns:
        mini_image

    """

    if len(image.shape) == 5:

        ip_ = np.ndarray((0,
                          int(zoom * image.shape[1]),
                          int(zoom * image.shape[2]),
                          int(zoom * image.shape[3]),
                          1))
        for bd in range(image.shape[0]):
            ic = ndimage.zoom(image[bd, :, :, :, 0], zoom)
            ic = np.expand_dims(np.expand_dims(ic, 0), -1)
            ip_ = np.concatenate((ip_, ic), axis=0)
    elif len(image.shape) == 4:
        ip_ = ndimage.zoom(image[:, :, :, 0], zoom)
        ip_ = np.expand_dims(np.expand_dims(ip_, 0), -1)
    else:
        raise Exception("Incorrect input! Input should have 4 or 5 dim "
                        "(d1 x d2 x d2 x channels) or (batch_size x d1 x d2 x d2 x channels)")
    return ip_


def get_cropped_image(iarr, boundaries, target_shape=(150, 150, 180), imap=None) -> tuple:
    """ Crops the original and mask records based on the boundary center resulting target shape size outputs
    Args:
        iarr: MR record
        boundaries: cropper predicted boundaries with length=6
        target_shape: target shape of the output
        imap (optional, default=None): MR mask or None

    Returns:
        cropped iarr, cropped_imap
    """

    assert boundaries.shape[0] == iarr.shape[0], "Inconsistent batch size"

    center = boundaries[:, :3] + (boundaries[:, 3:] / 2).round(0).astype(int)

    iarr_out = np.zeros((boundaries.shape[0], target_shape[0], target_shape[1], target_shape[2], iarr.shape[4]))

    if imap is not None:
        assert boundaries.shape[0] == imap.shape[0], "Inconsistent batch size"
        imap_out = np.zeros((boundaries.shape[0], target_shape[0], target_shape[1], target_shape[2]))
    else:
        imap_out = None

    for batch_index in range(iarr.shape[0]):
        bx1, bx2 = get_padded_boundaries(center=center[batch_index, 0],
                                         target_shape=target_shape[0],
                                         orig_array_shape=iarr.shape[1])
        by1, by2 = get_padded_boundaries(center=center[batch_index, 1],
                                         target_shape=target_shape[1],
                                         orig_array_shape=iarr.shape[2])
        bz1, bz2 = get_padded_boundaries(center=center[batch_index, 2],
                                         target_shape=target_shape[2],
                                         orig_array_shape=iarr.shape[3])

        iarr_out[batch_index, :, :, :, :] = iarr[batch_index,
                                            int(bx1):int(bx2),
                                            int(by1):int(by2),
                                            int(bz1):int(bz2),
                                            :]
        if imap is not None:
            imap_out[batch_index, :, :, :] = imap[batch_index,
                                             int(bx1):int(bx2),
                                             int(by1):int(by2),
                                             int(bz1):int(bz2)]

    return iarr_out, imap_out
