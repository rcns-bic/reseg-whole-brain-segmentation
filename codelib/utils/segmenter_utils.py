import numpy as np


def subvolume_sampling(iarr: np.ndarray,
                       imap=None,
                       distributer=2,
                       stepper_d=2):
    """ Subvolume sampling generator. distributer input defines the strides of sampling and the output shape:
    \t strides = image.shape/(2*distributer)
    \t output_shape = image.shape / distributer
    Args:
        iarr: mr record
        imap: mr mask
        distributer: distributer parameter of the sampling
        stepper_d (int, optional=2): it defines the step size (for 2, step size is inputshape/distributer/2)
    Returns:

    """
    assert (iarr.shape[1] % distributer == 0 and
            iarr.shape[2] % distributer == 0 and
            iarr.shape[3] % distributer == 0 and
            iarr.shape[1] % (stepper_d * distributer) == 0 and
            iarr.shape[2] % (stepper_d * distributer) == 0 and
            iarr.shape[3] % (stepper_d * distributer) == 0), "stepper_d * distributer should be the distributer of " \
                                                             "the array shape!"
    assert (iarr.shape[1] // (stepper_d * distributer) > 1 and
            iarr.shape[2] // (stepper_d * distributer) > 1 and
            iarr.shape[3] // (stepper_d * distributer) > 1), "stepper_d and/or distributer is too high."
    if imap is not None:
        assert imap.shape == iarr.shape, "Input arrays have different shape!"

    target_shape = (
        int(iarr.shape[1] / distributer), int(iarr.shape[2] / distributer), int(iarr.shape[3] / distributer))
    strides = (int(iarr.shape[1] / (stepper_d * distributer)),
               int(iarr.shape[2] / (stepper_d * distributer)),
               int(iarr.shape[3] / (stepper_d * distributer)))
    eps = 1
    if imap is not None:
        for idx in range(0, iarr.shape[1] - target_shape[0] + eps, strides[0]):
            for jdx in range(0, iarr.shape[2] - target_shape[1] + eps, strides[1]):
                for kdx in range(0, iarr.shape[3] - target_shape[2] + eps, strides[2]):
                    yield (
                        iarr[:, idx: idx + target_shape[0], jdx: jdx + target_shape[1], kdx:kdx + target_shape[2], :],
                        imap[:, idx: idx + target_shape[0], jdx: jdx + target_shape[1], kdx:kdx + target_shape[2]])
    else:
        for idx in range(0, iarr.shape[1] - target_shape[0] + eps, strides[0]):
            for jdx in range(0, iarr.shape[2] - target_shape[1] + eps, strides[1]):
                for kdx in range(0, iarr.shape[3] - target_shape[2] + eps, strides[2]):
                    yield (
                        iarr[:, idx: idx + target_shape[0], jdx: jdx + target_shape[1], kdx:kdx + target_shape[2], :],
                        None)


def subvolume_sampling_w_rebuild(
    iarr: np.ndarray,
    distributer=2,
    stepper_d=2):
    """ Subvolume sampling generator. distributer input defines the strides of sampling and the output shape:
    \t strides = image.shape/(2*distributer)
    \t output_shape = image.shape / distributer
    Args:
        iarr: mr record
        imap: mr mask
        distributer: distributer parameter of the sampling
        stepper_d (int, optional=2): it defines the step size (for 2, step size is inputshape/distributer/2)
    Returns:

    """
    assert (iarr.shape[1] % distributer == 0 and
            iarr.shape[2] % distributer == 0 and
            iarr.shape[3] % distributer == 0 and
            iarr.shape[1] % (stepper_d * distributer) == 0 and
            iarr.shape[2] % (stepper_d * distributer) == 0 and
            iarr.shape[3] % (stepper_d * distributer) == 0), "stepper_d * distributer should be the distributer of " \
                                                             "the array shape!"

    target_shape = (
        int(iarr.shape[1] / distributer), int(iarr.shape[2] / distributer), int(iarr.shape[3] / distributer))
    strides = (int(iarr.shape[1] / (stepper_d * distributer)),
               int(iarr.shape[2] / (stepper_d * distributer)),
               int(iarr.shape[3] / (stepper_d * distributer)))
    eps = 1
    for idx in range(0, iarr.shape[1] - target_shape[0] + eps, strides[0]):
        for jdx in range(0, iarr.shape[2] - target_shape[1] + eps, strides[1]):
            for kdx in range(0, iarr.shape[3] - target_shape[2] + eps, strides[2]):
                yield (
                    iarr[:, idx: idx + target_shape[0], jdx: jdx + target_shape[1], kdx:kdx + target_shape[2], :],
                    idx,
                    jdx,
                    kdx)

