import argparse
import glob
import os
import sys
import time

import nibabel as nib
import numpy as np
import tensorflow as tf

from codelib.utils import format_and_check_path
from codelib.utils.cropper_utils import get_cropped_image, batch_zoom
from codelib.utils.mri_convert import MriConform
from codelib.utils.segmenter_utils import subvolume_sampling_w_rebuild
from models.cropper import cropper_model as cropper_model
from models.segmenter import reseg_net as segmenter_model

parser = argparse.ArgumentParser()
parser.add_argument("--i", type=str, help="The input folder.", )
parser.add_argument("--o", type=str, default="./",
                    help="The save path where the masks will be saved.")
parser.add_argument("--filename_template", type=str, default=None, help="If test_file(s) list contains sub-folder, "
                                                                        "it defines the name of files / "
                                                                        "template for files (glob library based)")
parser.add_argument("--id_template", type=str, default="./__id__.nii.gz",
                    help="Help template to get id from the input filename '__id__' defines id place and the start "
                         "symbol before and end symbol after __id__ should define the place clearly. (Example: files "
                         "to segment: './main_folder/subfolders/someID.nii.gz -> Ok template: "
                         "'./main_folder/changing_folder_name/__id__.nii.gz')")
parser.add_argument("--save_conformed",
                    action="store_true", help="Also saves the conformed record.")
parser.add_argument("--save_entropy",
                    action="store_true",
                    help="Also saves the pixelwise entropies for the segmentation.")

parser.add_argument("--segmenter_path",
                    default="./data/models/segmenter/",
                    help="Segmenter model path")
parser.add_argument("--cropper_path",
                    default="./data/models/cropper/",
                    help="Cropper model path")
args = parser.parse_args()


def standardize(iarr):
    iarr = iarr - np.mean(iarr)
    iarr = iarr / np.std(iarr)
    return iarr


def save_masks(save_file_path,  # path and filename without postfix
               prediction,
               boundaries,
               target_shape=(256, 256, 256, 50),
               src_shape=(152, 152, 184),
               mgh_header=None,
               save_entropies=False):
    prediction = prediction.squeeze()
    padded = np.zeros((target_shape[0], target_shape[1], target_shape[2], target_shape[3]))
    padded[:, :, :, 0] = 1

    center = boundaries[:, :3] + (boundaries[:, 3:] / 2).round(0).astype(int)

    padded[int(center[0, 0] - src_shape[0] / 2):int(center[0, 0] + src_shape[0] / 2),
    int(center[0, 1] - src_shape[1] / 2):int(center[0, 1] + src_shape[1] / 2),
    int(center[0, 2] - src_shape[2] / 2):int(center[0, 2] + src_shape[2] / 2),
    :] = prediction

    # entropy
    if save_entropies:
        p_copy = padded.copy()
        maxes = p_copy.sum(-1)
        for idx in range(p_copy.shape[-1]):
            p_copy[:, :, :, idx] = p_copy[:, :, :, idx] / maxes

        entropies = -np.sum(((p_copy + 1e-9) * np.log(p_copy + 1e-9)), axis=-1)
        entropy_image = nib.MGHImage(entropies, mgh_header.get_affine(), mgh_header)
        nib.save(entropy_image, save_file_path + "_ent.mgz")

    prediction = np.argmax(padded, -1).astype(np.uint8)

    new_img = nib.MGHImage(prediction, mgh_header.get_affine(), mgh_header)
    nib.save(new_img, save_file_path + "_seg.mgz")


def segment_file(test_file_name,
                 id_template: dict,
                 save_path: str,
                 save_conformed_t1: bool,
                 save_entropies: bool):
    print("\n")
    # config
    devices = tf.config.list_physical_devices('GPU')
    if len(devices) == 0:
        print("GPU device not found!")
        device_name = "CPU:0"
    else:
        device_name = "GPU:0"
    print(f"Use device: {device_name}")

    # get file_id
    try:
        spl_filename = test_file_name.split(id_template["start_symbol"])
        file_id = spl_filename[id_template["split_place"]].split(id_template["end_symbol"])[0]
    except:
        raise Exception("Can't get file id based on the template!")

    # read and conform files
    try:
        # if file is nifti -> convert to mgh orientation
        if (test_file_name.split(".")[-1] == "nii" or
                ".".join(test_file_name.split(".")[-2:]) == "nii.gz"):

            conformer = MriConform(test_file_name)
            mgh_header = conformer.get_transformed_mgz_header()
            iarr = conformer.conform()
        # if file is mgh -> copy
        elif (test_file_name.split(".")[-1] == "mgh" or
              test_file_name.split(".")[-1] == "mgz"):
            read_file_g = nib.load(test_file_name)
            iarr = read_file_g.get_fdata()
            mgh_header = read_file_g.header
        else:
            raise Exception("Incorrect input data extension.")
    except:
        print("\nUnable to read/conform '{}'".format(test_file_name))
        return False

    # segment
    try:
        iarr = standardize(iarr)

        # 1, save conformed
        if save_conformed_t1:
            cim = nib.MGHImage(iarr, mgh_header.get_affine(), mgh_header)
            nib.save(cim, save_path + file_id + "_conformed.mgz")

        iarr = np.expand_dims(iarr, 0)
        iarr_raw = np.expand_dims(iarr, -1)

        ## pipeline
        mini_image = batch_zoom(iarr_raw, 0.5)
        with tf.device(f'/device:{device_name}'):

            boundaries = np.round(
                cropper.predict(mini_image),
                # cropper predicts normalized boundaries -> convert back to normal values
                0).astype(int)  # round data to integers
            iarr, _ = get_cropped_image(iarr=iarr_raw,
                                        imap=None,
                                        boundaries=boundaries,
                                        target_shape=target_segmentation_shape)
            pred = np.zeros((iarr.shape[0], iarr.shape[1], iarr.shape[2], iarr.shape[3], 50))

            # rebuild mask from subvolumes
            for sub_im, d1_index, d2_index, d3_index in subvolume_sampling_w_rebuild(iarr=iarr,
                                                                                     distributer=2,
                                                                                     stepper_d=4):
                sub_im = standardize(sub_im)

                pred[:,
                d1_index: d1_index + target_shape[0],
                d2_index: d2_index + target_shape[1],
                d3_index: d3_index + target_shape[2],
                :] += segmenter.predict(sub_im)

        # 2, save masks
        save_masks(save_file_path=save_path + file_id,
                   prediction=pred,
                   boundaries=boundaries,
                   src_shape=target_segmentation_shape,
                   target_shape=(256, 256, 256, 50),
                   mgh_header=mgh_header,
                   save_entropies=save_entropies)
        return True
    except:
        print("\nError in segmentaion of '{}'".format(test_file_name))
        return False


def decode_fileset_and_segment(test_files,
                               save_path: str,
                               id_template="path/__id__.mgz",
                               save_conformed_t1=False,
                               filename_template=None,
                               save_entropies=False):
    """ The function generates the segmented results.
    Args:
        test_files (list or str): 1 file/container or a list of files/containers
        save_path (str): save path folder
        id_template (str): help template to get id from the input filename '__init__' defines id place and the start
            symbol before and end symbol after __init__ should define the place clearly
            (Example: files t segment: "./folder1/folder2/somethingID.nii.gz -> Ok template: "./folder1/changingfoldername/__id__.nii.gz")
        filename_template (str): if test_files list contains container folder it defines the name of files or template for files (glob library based)
        save_conformed_t1 (bool): save conformed input if True
        save_entropies (bool): True if save entropies else False

    Returns:

    """

    # check input
    if isinstance(test_files, str):
        test_files = [test_files]
    if not isinstance(test_files, list):
        raise Exception("'test_files' input should be a string or a list")

    # check template
    assert id_template.find(
        "__id__") != -1, "In 'id_template' input '__id__' shows the file id. Template should contain it!"

    # Function generates id based on template. __init__ defines the place of the id therefore start symbol
    # before __id__ and end symbol after should define the place clearly
    __id_place_start = id_template.find("__id__")
    id_template_dict = {"start_symbol": id_template[__id_place_start - 1],
                        "end_symbol": id_template[__id_place_start + 6]}
    __spl_template = id_template.split(id_template_dict["start_symbol"])
    id_template_dict["split_place"] = -1
    for idx in range(len(__spl_template)):
        if __spl_template[idx].find("__id__") != -1:
            id_template_dict["split_place"] = idx
            break

    number_of_record_to_segment = len(test_files)
    times = np.array([])

    if not os.path.exists(save_path):
        os.makedirs(save_path)
        print("\nSave path created.")

    for idx, test_file in enumerate(test_files):
        t0 = time.time()
        if idx <= -1:
            continue

        # identify image file
        if os.path.isfile(test_file):
            test_ff = test_file.replace("\\", "/")
            segment_file(test_file_name=test_ff,
                         id_template=id_template_dict,
                         save_path=save_path,
                         save_conformed_t1=save_conformed_t1,
                         save_entropies=save_entropies)
            times = np.append(times, time.time() - t0)
        else:
            if filename_template is not None:
                # if filename defined by glob template
                if filename_template.find("*") != -1:
                    __ok_filenames = glob.glob(test_file + "/" + filename_template)
                    if len(__ok_filenames) == 0:
                        raise Exception("Can't find file based on 'filename_template'!")
                    else:
                        number_of_record_to_segment += len(__ok_filenames) - 1
                        for elem in __ok_filenames:
                            test_ff = elem.replace("\\", "/")
                            segment_file(test_file_name=test_ff,
                                         id_template=id_template_dict,
                                         save_path=save_path,
                                         save_conformed_t1=save_conformed_t1,
                                         save_entropies=save_entropies)
                            times = np.append(times, time.time() - t0)
                else:
                    test_ff = (test_file + "/" + filename_template).replace("\\", "/")
                    segment_file(test_file_name=test_ff,
                                 id_template=id_template_dict,
                                 save_path=save_path,
                                 save_conformed_t1=save_conformed_t1,
                                 save_entropies=save_entropies)
                    times = np.append(times, time.time() - t0)
            else:
                raise Exception("Unable to find files!")

        number_of_done_records = len(times)
        sys.stdout.write(
            "\r Records DONE: {:3d}/{:3d}; Elapsed time: {:8.2f}s; Time back: {:8.2f}".format(
                number_of_done_records,
                number_of_record_to_segment,
                times.sum(),
                times.mean() * (number_of_record_to_segment - number_of_done_records)))

    print("\nAverage time per image: {:.5f}".format(times.mean()))


def find_data_to_segment(input_dir: str,
                         output_dir: str,
                         id_template: str,
                         filename_template: str,
                         save_conformed=True,
                         save_entropy=True, ):
    if os.path.isfile(input_dir):
        decode_fileset_and_segment(test_files=input_dir,
                                   save_path=output_dir,
                                   id_template=id_template,
                                   filename_template=filename_template,
                                   save_conformed_t1=save_conformed,
                                   save_entropies=save_entropy)
    elif os.path.isdir(input_dir):
        session_folders = glob.glob(input_dir + "*")

        # windows uses \\ instead /
        session_folders_ch = []
        for elem in session_folders:
            session_folders_ch.append(elem.replace("\\", "/"))

        decode_fileset_and_segment(test_files=session_folders_ch,
                                   save_path=output_dir,
                                   id_template=id_template,
                                   filename_template=filename_template,
                                   save_conformed_t1=save_conformed,
                                   save_entropies=save_entropy)


if __name__ == "__main__":
    segmenter_path, exist = format_and_check_path(args.segmenter_path)
    assert exist, "Segmenter model path ('segmenter_path') argument does not exist."

    cropper_path, exist = format_and_check_path(args.cropper_path)
    assert exist, "Cropper model path ('cropper_path') argument does not exist."

    # check the existence of the result dir
    save_path, path_exist = format_and_check_path(args.o)
    assert path_exist, "Save path ('o' argument) does not exist!"

    # check the existence of the input data
    input_path, path_exist = format_and_check_path(args.i)
    assert path_exist, "'i' path does not exist!"

    target_segmentation_shape = (152, 152, 184)  # can be divided 2^4

    target_shape = (int(target_segmentation_shape[0] / 2),
                    int(target_segmentation_shape[1] / 2),
                    int(target_segmentation_shape[2] / 2))  # generate_mini_image

    # load models
    cropper = cropper_model(base_filters=64, conv_activation=tf.keras.activations.swish)
    cropper.load_weights(cropper_path + "model")
    segmenter = segmenter_model(input_shape=(target_segmentation_shape[0] // 2,
                                             target_segmentation_shape[1] // 2,
                                             target_segmentation_shape[2] // 2, 1),
                                base_filters=136,
                                conv_activation=tf.keras.activations.swish)
    segmenter.load_weights(segmenter_path + "model")

    find_data_to_segment(
        input_dir=input_path,
        output_dir=save_path,
        id_template=args.id_template,
        filename_template=args.filename_template,
        save_conformed=args.save_conformed,
        save_entropy=args.save_entropy)
